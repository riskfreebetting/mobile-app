import React, { useState } from "react";

export const AppStateContext = React.createContext({
  webViewURI: "",
  loadWebViewer: () => {},
  isWebViewVisible: false,
  setWebViewVisible: () => {},
});

function AppStateProvider(props) {
  const [isWebViewVisible, setWebViewVisible] = useState(false);
  const [webViewURI, setWebViewURI] = useState("");

  const loadWebViewer = (uri) => {
    setWebViewURI(uri);
    setWebViewVisible(true);
  };

  return (
    <AppStateContext.Provider
      value={{
        webViewURI,
        loadWebViewer,
        isWebViewVisible,
        setWebViewVisible,
      }}
    >
      {props.children}
    </AppStateContext.Provider>
  );
}

export default AppStateProvider;
