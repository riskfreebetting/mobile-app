import React, { useEffect, useRef, useState } from "react";
import {
  StyleSheet,
  TextInput,
  TouchableWithoutFeedback,
  View,
} from "react-native";
import colors from "../config/colors";
import { fonts, fontSizes } from "../config/fonts";
import {
  convertNumberStringToNumber,
  convertNumberToString,
} from "../scripts/utils";
import RFBText from "./RFBText";

function MoneyInput(props) {
  return (
    <Input
      {...props}
      keyboardType="decimal-pad"
      textAlign="right"
      unit="€"
      convertTextToValue={convertNumberStringToNumber}
      convertValueToText={convertNumberToString}
    />
  );
}

function PercentageInput(props) {
  return (
    <Input
      {...props}
      keyboardType="decimal-pad"
      textAlign="right"
      unit="%"
      convertTextToValue={(v) => convertNumberStringToNumber(v) / 100}
      convertValueToText={(v) => convertNumberToString(v * 100)}
    />
  );
}

function Input(props) {
  const {
    color = colors.primary,
    convertValueToText = (v) => v,
    convertTextToValue = (v) => v,
  } = props;

  const formattedValue = convertValueToText(props.value);
  const [value, onChangeText] = useState(formattedValue);

  useEffect(() => onChangeText(formattedValue), [props.value]);

  const input = useRef();
  return (
    <TouchableWithoutFeedback onPress={() => input.current.focus()}>
      <View style={[styles.container, { borderColor: color }]}>
        <RFBText style={[styles.unitText, { color }]}>{props.unit}</RFBText>
        <TextInput
          {...props}
          onEndEditing={() => props.onEndEditing(convertTextToValue(value))}
          value={value}
          onChangeText={onChangeText}
          selectionColor={color}
          selectTextOnFocus
          editable
          ref={input}
          style={[styles.textInput, { color }]}
        />
      </View>
    </TouchableWithoutFeedback>
  );
}

const styles = StyleSheet.create({
  container: {
    borderStyle: "solid",
    borderWidth: 1,
    borderRadius: 5,
    width: "100%",
    paddingStart: 5,
    paddingVertical: 1,
    flexDirection: "row-reverse",
  },
  unitText: {
    marginStart: 2,
  },
  textInput: {
    fontSize: fontSizes.XS,
    fontFamily: fonts.standard,
  },
});

export { MoneyInput, PercentageInput };
