import React from "react";
import { StyleSheet, View } from "react-native";
import RFBSvgIcon from "./RFBSvgIcon";

import ComingSoonImage from "../assets/svg/images/comingSoon.svg";
import colors from "../config/colors";
import RFBText from "./RFBText";
import { fontSizes } from "../config/fonts";

function ComingSoon(props) {
  return (
    <View style={[styles.container, props.style]}>
      <View style={styles.titleContainer}>
        <RFBText style={styles.titleText}>Coming soon</RFBText>
        <RFBText style={styles.pointsText}>...</RFBText>
      </View>
      <RFBSvgIcon
        style={styles.image}
        icon={ComingSoonImage}
        fill={colors.secondary}
        fillSecondary={colors.primary}
        fill3={colors.accent3}
        fill4={colors.accent}
        fill5={colors.accent2}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    width: "100%",
  },
  image: {
    marginBottom: 5,
    width: "100%",
  },
  pointsText: {
    fontSize: fontSizes.XL,
    color: colors.accent,
    textAlignVertical: "center",
    textAlign: "center",
  },
  titleContainer: {
    flexDirection: "row",
    alignItems: "center",
    marginBottom: 25,
  },
  titleText: {
    fontSize: fontSizes.XL,
  },
});

export default ComingSoon;
