import React from "react";
import { StyleSheet, TouchableHighlight, View } from "react-native";
import colors from "../config/colors";
import { fontSizes } from "../config/fonts";
import RFBText from "./RFBText";

function RFBListItem({
  HeaderComponent = <></>,
  ContentComponent = <></>,
  FooterComponent = <></>,
  buttonText = "",
  onPress = () => {},
  backgroundColor = colors.secondary,
  footerColor = colors.background2,
  addShadow = false,
}) {
  return (
    <View
      style={[
        styles.container,
        { backgroundColor },
        addShadow && styles.shadow,
      ]}
    >
      <View style={styles.headerContainer}>{HeaderComponent}</View>
      <View style={styles.contentContainer}>{ContentComponent}</View>
      <View style={[styles.footerContainer, { backgroundColor: footerColor }]}>
        <View style={styles.footerComponent}>{FooterComponent}</View>
        <TouchableHighlight
          activeOpacity={0.6}
          underlayColor={colors.accent}
          onPress={onPress}
          style={styles.buttonHighlight}
        >
          <View style={styles.button}>
            <RFBText style={styles.buttonText}>{buttonText}</RFBText>
          </View>
        </TouchableHighlight>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  button: {
    backgroundColor: colors.accent,
    paddingVertical: 5,
    paddingHorizontal: 25,
    borderTopRightRadius: 5,
    borderTopLeftRadius: 20,
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 20,
  },
  buttonHighlight: {
    borderTopRightRadius: 5,
    borderTopLeftRadius: 20,
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 20,
  },
  buttonText: {
    color: colors.background,
    textAlign: "center",
    fontSize: fontSizes.S,
  },
  contentContainer: {
    paddingHorizontal: 10,
    paddingVertical: 3,
    justifyContent: "space-evenly",
  },
  container: {
    borderTopRightRadius: 20,
    borderTopLeftRadius: 20,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
  },
  footerContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingStart: 10,
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
  },
  footerComponent: {
    maxWidth: "80%",
  },
  headerContainer: {
    paddingVertical: 5,
    paddingHorizontal: 10,
  },
  shadow: {
    shadowColor: colors.black,
    shadowOffset: { width: 0, height: 3 },
    shadowOpacity: 0.5,
    shadowRadius: 3,
    elevation: 1,
    overflow: "visible",
    zIndex: 200,
  },
});

export default RFBListItem;
