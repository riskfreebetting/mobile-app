import React, { useContext, useState } from "react";
import { StyleSheet, TouchableOpacity } from "react-native";
import { AppStateContext } from "./AppStateProvider";
import colors from "../config/colors";
import { fontSizes } from "../config/fonts";
import RFBSvgIcon from "./RFBSvgIcon";
import RFBText from "./RFBText";

function RFBIconLink(props) {
  const { loadWebViewer } = useContext(AppStateContext);

  return (
    <>
      <TouchableOpacity
        {...props}
        onPress={() => loadWebViewer(props.href)}
        style={{
          alignItems: "center",
          justifyContent: "space-between",
          ...props.style,
        }}
      >
        <RFBSvgIcon style={styles.icon} icon={props.icon} />
        <RFBText style={styles.text}>{props.children}</RFBText>
      </TouchableOpacity>
    </>
  );
}

const styles = StyleSheet.create({
  icon: {
    width: "100%",
  },
  text: {
    textAlign: "center",
    fontSize: fontSizes.XXXS,
    textDecorationColor: colors.primary,
    textDecorationStyle: "solid",
    textDecorationLine: "underline",
    marginTop: 2,
  },
});

export default RFBIconLink;
