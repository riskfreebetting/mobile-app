import React from "react";
import { Platform, StyleSheet, Text, View } from "react-native";
import colors from "../config/colors";
import { fontSizes } from "../config/fonts";
import RFBText from "./RFBText";

function RFBSvgIcon(props) {
  if (Platform.OS === "web" || !props.icon)
    return (
      <View
        {...props}
        style={[
          styles.container,
          {
            backgroundColor: props.fill ?? colors.primary,
            width: props.width ?? 30,
            height: props.height ?? 30,
            borderRadius: props.width ?? 30,
          },
          props.style,
        ]}
      >
        <RFBText
          style={[
            styles.text,
            {
              color: props.fillSecondary ?? colors.secondary,
            },
          ]}
        >
          {props.alt}
          {props.text !== undefined && (
            <RFBText
              style={[
                styles.smallText,
                { color: props.fillSecondary ?? colors.secondary },
              ]}
            >
              {" "}
              ({props.text})
            </RFBText>
          )}
        </RFBText>
      </View>
    );
  else {
    const width = props.width || props.style?.width;
    const height = props.height || props.style?.height;

    return (
      <props.icon
        {...props}
        {...(width ? { width } : {})}
        {...(height ? { height } : {})}
      />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    justifyContent: "center",
    alignItems: "center",
  },
  smallText: {
    fontSize: fontSizes.XXXS,
  },
  text: {
    fontSize: fontSizes.M,
    fontWeight: "bold",
  },
});

export default RFBSvgIcon;
