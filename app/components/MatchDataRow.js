import React from "react";
import { StyleSheet, View } from "react-native";
import colors from "../config/colors";
import { fontSizes } from "../config/fonts";
import { convertOddsNumberToString } from "../scripts/utils";
import RFBText from "./RFBText";

function MatchDataRow({ rfb, outcome }) {
  const rfbOutcome = rfb.bets[0].outcomes[outcome.name];
  const bookmaker = rfbOutcome.bookmaker;

  return (
    <View style={styles.container}>
      <RFBText
        style={styles.bookmakerName}
        numberOfLines={1}
        adjustsFontSizeToFit
      >
        {bookmaker.name}
      </RFBText>
      <View style={styles.teamNames}>
        <RFBText style={styles.teamName} adjustsFontSizeToFit numberOfLines={1}>
          {rfb.match.teams[0].team.alternativeNames[bookmaker.id][0]}
        </RFBText>
        <RFBText style={styles.teamName} adjustsFontSizeToFit numberOfLines={1}>
          {rfb.match.teams[1].team.alternativeNames[bookmaker.id][0]}
        </RFBText>
      </View>
      <View style={styles.outcomeNameContainer}>
        <RFBText style={styles.outcomeName}>{outcome.name}</RFBText>
      </View>
      <View style={styles.oddsContainer}>
        <RFBText style={styles.odd}>
          {convertOddsNumberToString(rfbOutcome.odd)}
        </RFBText>
        {rfbOutcome.taxedOdd !== rfbOutcome.odd && (
          <RFBText style={styles.taxedOdd}>
            ({convertOddsNumberToString(rfbOutcome.taxedOdd)})
          </RFBText>
        )}
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  bookmakerName: {
    flex: 2.5,
    fontSize: fontSizes.S,
    paddingEnd: 5,
  },
  container: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    marginVertical: 3,
  },
  odd: {
    color: colors.accent2,
    justifyContent: "center",
    textAlign: "center",
  },
  oddsContainer: {
    flex: 1,
    justifyContent: "center",
    textAlign: "center",
  },
  outcomeName: {
    justifyContent: "center",
    textAlign: "center",
  },
  outcomeNameContainer: {
    flex: 1,
    justifyContent: "center",
    textAlign: "center",
  },
  taxedOdd: {
    color: colors.accent3,
    justifyContent: "center",
    textAlign: "center",
  },
  teamName: {},
  teamNames: {
    flex: 4,
  },
});

export default MatchDataRow;
