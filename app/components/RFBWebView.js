import React, { useContext, useState } from "react";
import { SafeAreaView, StyleSheet, TouchableOpacity, View } from "react-native";
import { WebView } from "react-native-webview";
import { AppStateContext } from "./AppStateProvider";
import RFBText from "./RFBText";
import RFBSvgIcon from "./RFBSvgIcon";
import colors from "../config/colors";
import { getStatusBarHeight } from "../scripts/utils";

function RFBWebView() {
  const { isWebViewVisible, setWebViewVisible, webViewURI } =
    useContext(AppStateContext);

  return isWebViewVisible ? (
    <SafeAreaView style={styles.safeAreaContainer}>
      <View style={styles.container}>
        <View style={styles.upperBar}>
          <TouchableOpacity
            activeOpacity={0.8}
            onPress={() => setWebViewVisible(false)}
          >
            <RFBText style={styles.closeButton}>X</RFBText>
          </TouchableOpacity>
        </View>
        <WebView source={{ uri: webViewURI }} />
      </View>
    </SafeAreaView>
  ) : null;
}

const styles = StyleSheet.create({
  closeButton: {
    padding: 20,
    width: 50,
    height: 50,
    backgroundColor: colors.background2,
    borderColor: colors.secondary,
    borderWidth: 4,
    borderRadius: 25,
    textAlign: "center",
  },
  container: {
    flex: 1,
    minHeight: "100%",
    minWidth: "100%",
  },
  safeAreaContainer: {
    position: "absolute",
    left: 0,
    right: 0,
    top: 0,
    flex: 1,
    marginTop: getStatusBarHeight(),
    zIndex: 10,
    width: "100%",
    height: "100%",
  },
  upperBar: {
    backgroundColor: colors.background,
    flexDirection: "row",
    justifyContent: "flex-end",
    height: "8%",
    alignItems: "center",
  },
});

export default RFBWebView;
