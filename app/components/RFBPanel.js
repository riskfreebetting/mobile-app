import React, { useEffect, useState } from "react";
import { Animated, StyleSheet, TouchableOpacity, View } from "react-native";

import RFBSvgIcon from "./RFBSvgIcon";
import RFBText from "./RFBText";

import colors from "../config/colors";

import ArrowIcon from "../assets/svg/icons/arrow.svg";
import { useSimpleAnimation } from "../scripts/animations";

export function OpenableRFBTextPanel(props) {
  const [isOpened, setOpened] = useState(props.isOpened);
  console.log(props.isOpened, isOpened);

  const [rotate, animateRotation] = useSimpleAnimation(["0deg", "90deg"]);

  const [scaleY, animateHeight] = useSimpleAnimation([0, 1]);

  // for setting opened from outside
  useEffect(() => setOpened(props.isOpened), [props.isOpened]);

  // for animation
  useEffect(() => {
    console.log(isOpened);
    if (isOpened) {
      animateRotation(1);
      animateHeight(1);
    } else {
      animateRotation(0);
      animateHeight(0);
    }
  }, [isOpened]);

  return (
    <View style={props.style}>
      <RFBTextPanel
        iconRotate={rotate}
        onPress={() => setOpened(!isOpened) && props?.onPress?.(!isOpened)}
        {...props}
      >
        {props.text}
      </RFBTextPanel>
      <Animated.View
        style={{
          height: scaleY,
          transform: [{ scaleY }],
        }}
      >
        {props.children}
      </Animated.View>
    </View>
  );
}
export function RFBTextPanel(props) {
  return (
    <RFBPanel {...props}>
      <RFBText style={{ color: props.color ?? colors.primary }}>
        {props.children}
      </RFBText>
    </RFBPanel>
  );
}

function RFBPanel(props) {
  return (
    <TouchableOpacity activeOpacity={0.6} {...props} style={styles.container}>
      <View style={styles.text}>{props.children}</View>
      <Animated.View
        style={{
          transform: [
            {
              rotate: props.iconRotate ?? "0deg",
            },
          ],
        }}
      >
        <RFBSvgIcon icon={ArrowIcon} fill={colors.primary} />
      </Animated.View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.background2,
    width: "100%",
    height: 35,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  text: {
    marginLeft: 15,
  },
});

export default RFBPanel;
