import React from "react";
import { StyleSheet, TouchableOpacity, View } from "react-native";
import colors from "../config/colors";
import RFBSvgIcon from "./RFBSvgIcon";

function MenuBar(props) {
  return (
    <View style={styles.container}>
      {props.items.map((item) => (
        <TouchableOpacity
          key={item.name}
          activeOpacity={0.1}
          onPress={() => props.onSelect(item.name)}
          style={styles.buttonHighlight}
        >
          {item.accentDesign
            ? renderAccentItem(item)
            : renderStandardItem(item, props.selected === item.name)}
        </TouchableOpacity>
      ))}
    </View>
  );
}

function renderAccentItem(item) {
  return (
    <View style={styles.itemContainer}>
      <View style={[styles.item, styles.accentItem]}>
        <RFBSvgIcon
          icon={item.icon}
          fill={colors.accent3}
          fillSecondary={colors.accent2}
          width={55}
          height={55}
          alt={item.name[0].toUpperCase()}
        />
      </View>
    </View>
  );
}

function renderStandardItem(item, isSelected) {
  return (
    <View
      style={[
        styles.itemContainer,
        isSelected ? styles.selectedItemContainer : null,
      ]}
    >
      <View style={styles.item}>
        <RFBSvgIcon
          icon={item.icon}
          fill={isSelected ? colors.accent : colors.primary}
          width={38}
          height={38}
          alt={item.name[0].toUpperCase()}
        />
      </View>
    </View>
  );
}

export default MenuBar;

const styles = StyleSheet.create({
  accentItem: {
    width: 55,
    height: 55,
    borderRadius: 55,
    alignSelf: "flex-start",
    bottom: 17,
  },
  buttonHighlight: {
    paddingHorizontal: 20,
    alignItems: "center",
  },
  container: {
    width: "100%",
    flexDirection: "row",
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    justifyContent: "space-around",
    backgroundColor: colors.secondary,
    height: 100,
    zIndex: 1,
  },
  item: {
    width: 38,
    height: 38,
    justifyContent: "center",
    alignItems: "center",
  },
  selectedItemContainer: {
    width: 38,
    borderTopWidth: 3,
    borderTopColor: colors.accent,
    paddingTop: 6,
  },
  itemContainer: {
    paddingTop: 8,
  },
});
