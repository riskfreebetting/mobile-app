import React from "react";
import { StyleSheet, View } from "react-native";
import colors from "../config/colors";
import { fontSizes } from "../config/fonts";
import RFBText from "./RFBText";

function MatchDataHeader({ rfb }) {
  return (
    <View style={styles.container}>
      <RFBText
        style={styles.competitionText}
        adjustsFontSizeToFit
        numberOfLines={1}
      >
        {rfb.match.competition.sport.name}, {rfb.match.competition.country.name}
        , {rfb.match.competition.name}
      </RFBText>
      <RFBText style={styles.dateText} adjustsFontSizeToFit numberOfLines={1}>
        {getFormattedDate(rfb.match.startDate)}
      </RFBText>
    </View>
  );
}

function getFormattedDate(date) {
  return new Date(date).toLocaleString().slice(0, -3);
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingVertical: 5,
    paddingHorizontal: 10,
  },
  competitionText: {
    color: colors.accent2,
    fontSize: fontSizes.XXS,
  },
  dateText: {
    color: colors.accent,
    fontSize: fontSizes.XXS,
  },
});

export default MatchDataHeader;
