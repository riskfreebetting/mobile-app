import React from "react";
import { StyleSheet, View } from "react-native";

import RFBSvgIcon from "./RFBSvgIcon";
import RFBText from "./RFBText";

import Logo from "../assets/svg/logos/logo.svg";

import colors from "../config/colors";
import { fontSizes } from "../config/fonts";

function TopBar({ buttons = [], style }) {
  return (
    <View style={[styles.container, style]}>
      <View style={styles.leftContainer}>
        <RFBSvgIcon
          style={styles.logo}
          icon={Logo}
          width={60}
          height={60}
          alt="logo"
        />
        <View style={styles.appNameContainer}>
          <RFBText style={styles.appNameText}>RFB</RFBText>
        </View>
      </View>
      <View style={styles.rightContainer}>
        {buttons.map((Button, i) => (
          <View key={i} style={styles.buttonContainer}>
            <Button />
          </View>
        ))}
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  appNameContainer: {
    backgroundColor: colors.accent2,
    marginHorizontal: 15,
    borderRadius: 10,
  },
  appNameText: {
    color: colors.background,
    fontSize: fontSizes.XL,
    paddingHorizontal: 15,
    paddingVertical: 8,
  },
  buttonContainer: {
    paddingHorizontal: 5,
  },
  container: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    margin: 5,
  },
  leftContainer: {
    flexDirection: "row",
    justifyContent: "space-evenly",
    alignItems: "center",
  },
  logo: {
    marginRight: 15,
    marginLeft: 5,
  },
  rightContainer: {
    flexDirection: "row",
    justifyContent: "flex-end",
    alignItems: "center",
    flex: 1,
    marginEnd: 10,
  },
});

export default TopBar;
