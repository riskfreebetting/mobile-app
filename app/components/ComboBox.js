import React, { useEffect, useRef } from "react";
import { StyleSheet, View } from "react-native";
import ModalDropdown from "react-native-modal-dropdown";
import colors from "../config/colors";
import { fonts, fontSizes } from "../config/fonts";
import RFBText from "./RFBText";

function ComboBox(props) {
  const { color, items } = props;

  const ref = useRef();
  useEffect(
    () =>
      ref.current.select(
        items.findIndex((value) => value === props.selectedValue)
      ),
    [props.selectedValue]
  );

  return (
    <ModalDropdown
      options={items}
      style={[styles.picker, { borderColor: color }]}
      dropdownStyle={styles.dropdown}
      textStyle={[styles.text, { color }]}
      onSelect={(idx, value) => props.onSelect(value)}
      renderRightComponent={() => <RFBText style={{ color }}>&#9660;</RFBText>}
      renderRow={(option, idx, isSelected) => (
        <View style={{ backgroundColor: colors.background, padding: 5 }}>
          <RFBText
            style={{ color, fontWeight: isSelected ? "bold" : "normal" }}
            numberOfLines={1}
          >
            {option}
          </RFBText>
        </View>
      )}
      renderSeparator={() => (
        <View style={{ height: 1, backgroundColor: colors.secondary }}></View>
      )}
      isFullWidth
      showsVerticalScrollIndicator
      ref={ref}
    />
  );
}

const styles = StyleSheet.create({
  dropdown: {
    backgroundColor: colors.background,
    borderWidth: 0,
  },
  picker: {
    width: "100%",
    borderStyle: "solid",
    borderWidth: 1,
    borderRadius: 5,
  },
  text: {
    paddingHorizontal: 5,
    width: "80%",
    fontFamily: fonts.standard,
    fontSize: fontSizes.XS,
  },
});

export default ComboBox;
