import React, { useState } from "react";
import { StyleSheet, View } from "react-native";
import FeedScreen from "../screens/FeedScreen/FeedScreen";
import SettingsScreen from "../screens/SettingsScreen/SettingsScreen";
import MenuBar from "./MenuBar";
import HomeIcon from "../assets/svg/icons/home.svg";
import StatsIcon from "../assets/svg/icons/stats.svg";
import InfoIcon from "../assets/svg/icons/info.svg";
import AboutUsIcon from "../assets/svg/icons/aboutUs.svg";
import TrackingScreen from "../screens/TrackingScreen/TrackingScreen";
import TutorialsScreen from "../screens/TutorialsScreen/TutorialsScreen";

const screens = [
  {
    name: "feed",
    icon: HomeIcon,
    component: FeedScreen,
  },
  {
    name: "tracking",
    icon: StatsIcon,
    component: TrackingScreen,
  } /* 
  {
    name: "premium",
    icon: StarIcon,
    accentDesign: true,
  }, */,
  {
    name: "tutorials",
    icon: InfoIcon,
    component: TutorialsScreen,
  },
  {
    name: "settings",
    icon: AboutUsIcon,
    component: SettingsScreen,
  },
];

function ScreenRouter() {
  const [selectedScreen, setSelectedScreen] = useState("feed");

  return (
    <View>
      {findSelectedScreenComponent(selectedScreen)}
      <MenuBar
        style={styles.menu}
        selected={selectedScreen}
        onSelect={setSelectedScreen}
        items={screens}
      />
    </View>
  );
}

function findSelectedScreenComponent(selectedScreen) {
  const Component = screens.find(
    (screen) => screen.name === selectedScreen
  )?.component;

  return <View style={styles.screen}>{Component ? <Component /> : null}</View>;
}

const styles = StyleSheet.create({
  screen: {
    minHeight: "90%",
    maxHeight: "90%",
  },
  menu: {
    minHeight: "10%",
    maxHeight: "10%",
  },
});

export default ScreenRouter;
