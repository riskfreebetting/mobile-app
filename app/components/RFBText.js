import React from "react";
import { Text } from "react-native";
import colors from "../config/colors";
import { fonts, fontSizes } from "../config/fonts";

function RFBText(props) {
  return (
    <Text
      {...props}
      style={[
        {
          color: colors.primary,
          fontFamily: hasStyle(props, "fontWeight", "bold")
            ? fonts.bold
            : fonts.standard,
          fontSize: fontSizes.XS,
        },
        props.style,
      ]}
    >
      {props.children}
    </Text>
  );
}

function hasStyle(props, styleName, value) {
  if (Array.isArray(props.style))
    return props.style?.some?.((style) => style[styleName] === value);
  else return props.style?.[styleName] === value;
}

export default RFBText;
