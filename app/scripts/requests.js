const url = "http://185.234.72.236";

const logMessage = ({ type, msg }) => {
  const logs = {
    E: console.error,
    W: console.warn,
    I: console.info,
  };

  const log = logs[type] || console.log;

  log(msg);
};

const handleRequest = (ending) => async () => {
  try {
    const res = await fetch(url + ending);
    const data = await res.json();

    data.messages.forEach(logMessage);

    return data.result;
  } catch (err) {
    console.error("RFB Request failed!", err);
  }
};

const fetchAvailableRFBs = handleRequest("/rfbs?db=dev&isavailable=true");
const fetchAllRFBs = handleRequest("/rfbs?db=dev1&isavailable=false");

const fetchAvailableRFBsIfHasAny = async () => {
  const available = await fetchAvailableRFBs();
  return available.length > 0 ? available : fetchAllRFBs();
};

module.exports = {
  fetchAvailableRFBs,
  fetchAllRFBs,
  fetchAvailableRFBsIfHasAny,
};

/* "infoPlist": {
  "NSAppTransportSecurity": {
    "NSExceptionDomains": {
      "185.234.72.236": {
        "NSTemporaryExceptionAllowsInsecureHTTPLoads": true
      }
    }
  }
} */
