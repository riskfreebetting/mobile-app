import { Dimensions, Platform, StatusBar } from "react-native";

function isIPhoneX() {
  const dimen = Dimensions.get("window");
  return (
    Platform.OS === "ios" &&
    !Platform.isPad &&
    !Platform.isTVOS &&
    (dimen.height === 812 ||
      dimen.width === 812 ||
      dimen.height === 896 ||
      dimen.width === 896)
  );
}

export function getStatusBarHeight() {
  return Platform.select({
    ios: isIPhoneX() ? 44 : 20,
    android: StatusBar.currentHeight,
    default: 0,
  });
}

export function isIOS() {
  return Platform.Os === "ios";
}

export function convertNumberStringToNumber(value) {
  return parseFloat(value.replace(",", ".") || 0);
}

export function convertNumberToString(value) {
  const string = value?.toFixed(2) ?? "";

  if (string.endsWith(".00")) return string.slice(0, -3);
  else return string;
}

export function convertOddsNumberToString(value) {
  const string = value?.toFixed(2) ?? "";

  if (string.endsWith(".00")) return string.slice(0, -3);
  else if (string.includes(".") && string.endsWith("0"))
    return string.slice(0, -1);
  else return string;
}

export const sum = (fn) => (arr) =>
  arr.reduce((acc, curr) => acc + fn(curr), 0);

export const getBookmakerName = (outcomeName) => (bet) =>
  bet.outcomes[outcomeName].bookmaker.name;
