import { useRef } from "react";
import { Animated } from "react-native";

export const useSimpleAnimation = (outputRange, inputRange = [0, 1]) => {
  const animation = useRef(new Animated.Value(0)).current;
  const value = animation.interpolate({
    inputRange,
    outputRange,
  });

  const animate = (toValue) =>
    Animated.timing(animation, {
      toValue,
      duration: 500,
      useNativeDriver: false,
    }).start();
  return [value, animate];
};
