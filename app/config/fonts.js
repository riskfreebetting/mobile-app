import { Rubik_500Medium, Rubik_700Bold } from "@expo-google-fonts/rubik";

export const fontsToLoad = {
  Rubik: Rubik_500Medium,
  RubikBold: Rubik_700Bold,
};

export const fonts = {
  standard: "Rubik",
  bold: "RubikBold",
};

export const fontSizes = {
  XXXS: 10,
  XXS: 12,
  XS: 14,
  S: 18,
  M: 20,
  L: 24,
  XL: 36,
  XXL: 48,
  XXXL: 64,
};
