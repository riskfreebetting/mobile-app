export default {
  primary: "#BDDAE5",
  secondary: "#252A4A",
  background: "#090D28",
  background2: "#181B3A",
  accent: "#02D294",
  accent2: "#D2B102",
  accent3: "#E23A58",
  white: "#fff",
  black: "#000",
};
