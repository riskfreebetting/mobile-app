export default [
  {
    id: "39ef5d5c-9134-4125-9f36-479f75878a81",
    isAvailable: true,
    match: {
      id: "45b1484f-751f-4862-ad5e-1322d0e79965",
      name: "Borussia Dortmund - FC Bayern",
      result: null,
      status: "NS",
      currentMinute: null,
      startDate: 1607632035000,
      endDate: null,
      odds: [
        {
          id: "95b87467-2690-491f-9f46-d488fbdef597",
          name: "Odds 95b87467-2690-491f-9f46-d488fbdef597",
          bookmaker: {
            id: "31504e64-98d6-445e-8392-b1b14c01bb4d",
            tax: "5% WINNINGS",
            name: "Bwin",
          },
          bets: [
            {
              id: "785e2120-a43b-4e95-b486-058d399c3cb6",
              date: 1607623700000,
              type: {
                id: "592fecca-9a9b-4ea0-bb89-5c9078c946c5",
                name: "Tendency after Regular Playing Time",
                outcomeNames: [
                  {
                    id: "b625073e-099b-4876-bb70-9dd112c47a6a",
                    name: "1",
                  },
                  {
                    id: "2972466c-6952-4c80-888b-c840942456ec",
                    name: "X",
                  },
                  {
                    id: "c756965f-9a64-4935-89d8-124de0f7f698",
                    name: "2",
                  },
                ],
              },
              outcomes: [
                {
                  name: "b625073e-099b-4876-bb70-9dd112c47a6a",
                  odd: 2.1,
                  date: 1607629700000,
                },
                {
                  name: "2972466c-6952-4c80-888b-c840942456ec",
                  odd: 3.4,
                  date: 1607629700000,
                },
                {
                  name: "c756965f-9a64-4935-89d8-124de0f7f698",
                  odd: 2.6,
                  date: 1607629700000,
                },
              ],
            },
          ],
        },
        {
          id: "7bf63aee-8e08-41d2-9779-7a69e76e1135",
          name: "Odds 7bf63aee-8e08-41d2-9779-7a69e76e1135",
          bookmaker: {
            id: "16642f69-68c6-4bed-88db-c4e6c33c737f",
            tax: "NONE",
            name: "Tipico",
          },
          bets: [
            {
              id: "19dedd97-5c60-4ca7-a305-bf01e5641166",
              date: 1607623700000,
              type: {
                id: "592fecca-9a9b-4ea0-bb89-5c9078c946c5",
                name: "Tendency after Regular Playing Time",
                outcomeNames: [
                  {
                    id: "b625073e-099b-4876-bb70-9dd112c47a6a",
                    name: "1",
                  },
                  {
                    id: "2972466c-6952-4c80-888b-c840942456ec",
                    name: "X",
                  },
                  {
                    id: "c756965f-9a64-4935-89d8-124de0f7f698",
                    name: "2",
                  },
                ],
              },
              outcomes: [
                {
                  name: "b625073e-099b-4876-bb70-9dd112c47a6a",
                  odd: 3,
                  date: 1607629700000,
                },
                {
                  name: "2972466c-6952-4c80-888b-c840942456ec",
                  odd: 2.9,
                  date: 1607629700000,
                },
                {
                  name: "c756965f-9a64-4935-89d8-124de0f7f698",
                  odd: 3,
                  date: 1607629700000,
                },
              ],
            },
          ],
        },
      ],
      teams: [
        {
          id: "df380ea0-70f9-4a15-a5b9-ae3654d02c75",
          name: "Borussia Dortmund",
          alternativeNames: {
            "31504e64-98d6-445e-8392-b1b14c01bb4d": ["Borussia Dortmund"],
            "16642f69-68c6-4bed-88db-c4e6c33c737f": ["BORUSSIA DORTMUND"],
          },
          teamId: "7e81b45e-3fb7-468c-85e0-ff68188c8233",
          matchId: "45b1484f-751f-4862-ad5e-1322d0e79965",
        },
        {
          id: "1b1b2cba-4a8f-40c0-a8b4-15747554270c",
          name: "FC Bayern",
          alternativeNames: {
            "31504e64-98d6-445e-8392-b1b14c01bb4d": ["FC Bayern"],
            "16642f69-68c6-4bed-88db-c4e6c33c737f": ["BAYERN MÜNCHEN"],
          },
          teamId: "10c59325-6ba4-48ae-b17d-ae8685d36d9d",
          matchId: "45b1484f-751f-4862-ad5e-1322d0e79965",
        },
      ],
      competition: {
        id: "46e6090e-ec84-480b-9d37-d8f1e27d0f78",
        name: "Bundesliga",
        alternativeNames: {
          "aaf4ea31-c643-4142-adce-e3577118adfc": ["Bundesliga"],
        },
        sport: {
          id: "7dca3c22-5dda-4806-8f09-a355b77f6bcc",
          name: "Fußball",
          alternativeNames: {
            "31504e64-98d6-445e-8392-b1b14c01bb4d": ["Fußball"],
          },
        },
        country: {
          id: "a2b913d0-77ba-43b2-a8a6-123e525cd68b",
          name: "Deutschland",
          alternativeNames: {
            "31504e64-98d6-445e-8392-b1b14c01bb4d": ["Deutschland"],
          },
        },
      },
    },
    betType: {
      id: "592fecca-9a9b-4ea0-bb89-5c9078c946c5",
      name: "Tendency after Regular Playing Time",
      outcomeNames: [
        {
          id: "b625073e-099b-4876-bb70-9dd112c47a6a",
          name: "1",
        },
        {
          id: "2972466c-6952-4c80-888b-c840942456ec",
          name: "X",
        },
        {
          id: "c756965f-9a64-4935-89d8-124de0f7f698",
          name: "2",
        },
      ],
    },
    bets: [
      {
        profit: 0.0893,
        isAvailable: false,
        outcomes: {
          1: {
            bookmaker: {
              id: "31504e64-98d6-445e-8392-b1b14c01bb4d",
              tax: "5% WINNINGS",
              name: "Bwin",
            },
            odd: 3,
          },
          2: {
            bookmaker: {
              id: "31504e64-98d6-445e-8392-b1b14c01bb4d",
              tax: "5% WINNINGS",
              name: "Bwin",
            },
            odd: 2.6,
          },
          X: {
            bookmaker: {
              id: "16642f69-68c6-4bed-88db-c4e6c33c737f",
              tax: "NONE",
              name: "Tipico",
            },
            odd: 5,
          },
        },
      },
      {
        profit: 0.04945799458,
        isAvailable: false,
        outcomes: {
          1: {
            bookmaker: {
              id: "31504e64-98d6-445e-8392-b1b14c01bb4d",
              tax: "5% WINNINGS",
              name: "Bwin",
            },
            odd: 1.8,
          },
          2: {
            bookmaker: {
              id: "31504e64-98d6-445e-8392-b1b14c01bb4d",
              tax: "5% WINNINGS",
              name: "Bwin",
            },
            odd: 4,
          },
          X: {
            bookmaker: {
              id: "16642f69-68c6-4bed-88db-c4e6c33c737f",
              tax: "NONE",
              name: "Tipico",
            },
            odd: 4.1,
          },
        },
      },
      {
        profit: 0.04081632653061229,
        isAvailable: true,
        outcomes: {
          1: {
            bookmaker: {
              id: "16642f69-68c6-4bed-88db-c4e6c33c737f",
              tax: "NONE",
              name: "Tipico",
            },
            odd: 3,
          },
          2: {
            bookmaker: {
              id: "16642f69-68c6-4bed-88db-c4e6c33c737f",
              tax: "NONE",
              name: "Tipico",
            },
            odd: 3,
          },
          X: {
            bookmaker: {
              id: "31504e64-98d6-445e-8392-b1b14c01bb4d",
              tax: "5% WINNINGS",
              name: "Bwin",
            },
            odd: 3.4,
          },
        },
      },
    ],
  },
  {
    id: "49ef5d5c-9134-4125-9f36-479f75878a82",
    isAvailable: true,
    match: {
      id: "55b1484f-751f-4862-ad5e-1322d0e79966",
      name: "Real Madrid - FC Barcelona",
      result: null,
      status: "NS",
      currentMinute: null,
      startDate: 1620852557836,
      endDate: null,
      odds: [
        {
          id: "95b87467-2690-491f-9f46-d488fbdef597",
          name: "Odds 95b87467-2690-491f-9f46-d488fbdef597",
          bookmaker: {
            id: "31504e64-98d6-445e-8392-b1b14c01bb4d",
            tax: "5% WINNINGS",
            name: "Bwin",
          },
          bets: [
            {
              id: "785e2120-a43b-4e95-b486-058d399c3cb6",
              date: 1620752557836,
              type: {
                id: "592fecca-9a9b-4ea0-bb89-5c9078c946c5",
                name: "Tendency after Regular Playing Time",
                outcomeNames: [
                  {
                    id: "b625073e-099b-4876-bb70-9dd112c47a6a",
                    name: "1",
                  },
                  {
                    id: "2972466c-6952-4c80-888b-c840942456ec",
                    name: "X",
                  },
                  {
                    id: "c756965f-9a64-4935-89d8-124de0f7f698",
                    name: "2",
                  },
                ],
              },
              outcomes: [
                {
                  name: "b625073e-099b-4876-bb70-9dd112c47a6a",
                  odd: 3.3,
                  date: 1620752557836,
                },
                {
                  name: "2972466c-6952-4c80-888b-c840942456ec",
                  odd: 4,
                  date: 1620752557836,
                },
                {
                  name: "c756965f-9a64-4935-89d8-124de0f7f698",
                  odd: 2.2,
                  date: 1620752557836,
                },
              ],
            },
          ],
        },
        {
          id: "7bf63aee-8e08-41d2-9779-7a69e76e1135",
          name: "Odds 7bf63aee-8e08-41d2-9779-7a69e76e1135",
          bookmaker: {
            id: "16642f69-68c6-4bed-88db-c4e6c33c737f",
            tax: "NONE",
            name: "Tipico",
          },
          bets: [
            {
              id: "19dedd97-5c60-4ca7-a305-bf01e5641166",
              date: 1620752557836,
              type: {
                id: "592fecca-9a9b-4ea0-bb89-5c9078c946c5",
                name: "Tendency after Regular Playing Time",
                outcomeNames: [
                  {
                    id: "b625073e-099b-4876-bb70-9dd112c47a6a",
                    name: "1",
                  },
                  {
                    id: "2972466c-6952-4c80-888b-c840942456ec",
                    name: "X",
                  },
                  {
                    id: "c756965f-9a64-4935-89d8-124de0f7f698",
                    name: "2",
                  },
                ],
              },
              outcomes: [
                {
                  name: "b625073e-099b-4876-bb70-9dd112c47a6a",
                  odd: 3.5,
                  date: 1620752557836,
                },
                {
                  name: "2972466c-6952-4c80-888b-c840942456ec",
                  odd: 4.4,
                  date: 1620752557836,
                },
                {
                  name: "c756965f-9a64-4935-89d8-124de0f7f698",
                  odd: 1.8,
                  date: 1620752557836,
                },
              ],
            },
          ],
        },
      ],
      teams: [
        {
          id: "df380ea0-70f9-4a15-a5b9-ae3654d02c75",
          name: "Real Madrid",
          team: {
            alternativeNames: {
              "31504e64-98d6-445e-8392-b1b14c01bb4d": ["Real Madrid"],
              "16642f69-68c6-4bed-88db-c4e6c33c737f": ["REAL MADRID"],
            },
          },
          matchId: "45b1484f-751f-4862-ad5e-1322d0e79965",
        },
        {
          id: "1b1b2cba-4a8f-40c0-a8b4-15747554270c",
          name: "FC Barcelona",
          alternativeNames: {
            "31504e64-98d6-445e-8392-b1b14c01bb4d": ["FC Barcelona"],
            "16642f69-68c6-4bed-88db-c4e6c33c737f": ["FC BARCELONA"],
          },
          teamId: "10c59325-6ba4-48ae-b17d-ae8685d36d9d",
          matchId: "45b1484f-751f-4862-ad5e-1322d0e79965",
        },
      ],
      competition: {
        id: "46e6090e-ec84-480b-9d37-d8f1e27d0f79",
        name: "La Liga",
        alternativeNames: {
          "16642f69-68c6-4bed-88db-c4e6c33c737f": ["La Liga"],
          "aaf4ea31-c643-4142-adce-e3577118adfc": ["LA LIGA"],
        },
        sport: {
          id: "7dca3c22-5dda-4806-8f09-a355b77f6bcc",
          name: "Fußball",
          alternativeNames: {
            "16642f69-68c6-4bed-88db-c4e6c33c737f": ["FUSSBALL"],
            "31504e64-98d6-445e-8392-b1b14c01bb4d": ["Fußball"],
          },
        },
        country: {
          id: "a2b913d0-77ba-43b2-a8a6-123e525cd68b",
          name: "Spanien",
          alternativeNames: {
            "16642f69-68c6-4bed-88db-c4e6c33c737f": ["SPANIEN"],
            "31504e64-98d6-445e-8392-b1b14c01bb4d": ["Spanien"],
          },
        },
      },
    },
    betType: {
      id: "592fecca-9a9b-4ea0-bb89-5c9078c946c5",
      name: "Tendency after Regular Playing Time",
      outcomeNames: [
        {
          id: "b625073e-099b-4876-bb70-9dd112c47a6a",
          name: "1",
        },
        {
          id: "2972466c-6952-4c80-888b-c840942456ec",
          name: "X",
        },
        {
          id: "c756965f-9a64-4935-89d8-124de0f7f698",
          name: "2",
        },
      ],
    },
    bets: [
      {
        profit: 0.0153846,
        isAvailable: true,
        outcomes: {
          1: {
            bookmaker: {
              id: "31504e64-98d6-445e-8392-b1b14c01bb4d",
              tax: "5% WINNINGS",
              name: "Bwin",
            },
            odd: 3.3,
          },
          2: {
            bookmaker: {
              id: "31504e64-98d6-445e-8392-b1b14c01bb4d",
              tax: "5% WINNINGS",
              name: "Bwin",
            },
            odd: 2.2,
          },
          X: {
            bookmaker: {
              id: "16642f69-68c6-4bed-88db-c4e6c33c737f",
              tax: "NONE",
              name: "Tipico",
            },
            odd: 4.4,
          },
        },
      },
    ],
  },
  {
    id: "49ef5d5c-9134-4125-9f36-479f75878a83",
    isAvailable: true,
    match: {
      id: "55b1484f-751f-4862-ad5e-1322d0e79966",
      name: "Real Madrid - FC Barcelona",
      result: null,
      status: "NS",
      currentMinute: null,
      startDate: 1620852557836,
      endDate: null,
      odds: [
        {
          id: "95b87467-2690-491f-9f46-d488fbdef597",
          name: "Odds 95b87467-2690-491f-9f46-d488fbdef597",
          bookmaker: {
            id: "31504e64-98d6-445e-8392-b1b14c01bb4d",
            tax: "5% WINNINGS",
            name: "Bwin",
          },
          bets: [
            {
              id: "785e2120-a43b-4e95-b486-058d399c3cb6",
              date: 1620752557836,
              type: {
                id: "592fecca-9a9b-4ea0-bb89-5c9078c946c5",
                name: "Tendency after Regular Playing Time",
                outcomeNames: [
                  {
                    id: "b625073e-099b-4876-bb70-9dd112c47a6a",
                    name: "1",
                  },
                  {
                    id: "2972466c-6952-4c80-888b-c840942456ec",
                    name: "X",
                  },
                  {
                    id: "c756965f-9a64-4935-89d8-124de0f7f698",
                    name: "2",
                  },
                ],
              },
              outcomes: [
                {
                  name: "b625073e-099b-4876-bb70-9dd112c47a6a",
                  odd: 3.3,
                  date: 1620752557836,
                },
                {
                  name: "2972466c-6952-4c80-888b-c840942456ec",
                  odd: 4,
                  date: 1620752557836,
                },
                {
                  name: "c756965f-9a64-4935-89d8-124de0f7f698",
                  odd: 2.2,
                  date: 1620752557836,
                },
              ],
            },
          ],
        },
        {
          id: "7bf63aee-8e08-41d2-9779-7a69e76e1135",
          name: "Odds 7bf63aee-8e08-41d2-9779-7a69e76e1135",
          bookmaker: {
            id: "16642f69-68c6-4bed-88db-c4e6c33c737f",
            tax: "NONE",
            name: "Tipico",
          },
          bets: [
            {
              id: "19dedd97-5c60-4ca7-a305-bf01e5641166",
              date: 1620752557836,
              type: {
                id: "592fecca-9a9b-4ea0-bb89-5c9078c946c5",
                name: "Tendency after Regular Playing Time",
                outcomeNames: [
                  {
                    id: "b625073e-099b-4876-bb70-9dd112c47a6a",
                    name: "1",
                  },
                  {
                    id: "2972466c-6952-4c80-888b-c840942456ec",
                    name: "X",
                  },
                  {
                    id: "c756965f-9a64-4935-89d8-124de0f7f698",
                    name: "2",
                  },
                ],
              },
              outcomes: [
                {
                  name: "b625073e-099b-4876-bb70-9dd112c47a6a",
                  odd: 3.5,
                  date: 1620752557836,
                },
                {
                  name: "2972466c-6952-4c80-888b-c840942456ec",
                  odd: 4.4,
                  date: 1620752557836,
                },
                {
                  name: "c756965f-9a64-4935-89d8-124de0f7f698",
                  odd: 1.8,
                  date: 1620752557836,
                },
              ],
            },
          ],
        },
      ],
      teams: [
        {
          id: "df380ea0-70f9-4a15-a5b9-ae3654d02c75",
          name: "Real Madrid",
          alternativeNames: {
            "31504e64-98d6-445e-8392-b1b14c01bb4d": ["Real Madrid"],
            "16642f69-68c6-4bed-88db-c4e6c33c737f": ["REAL MADRID"],
          },
          teamId: "7e81b45e-3fb7-468c-85e0-ff68188c8233",
          matchId: "45b1484f-751f-4862-ad5e-1322d0e79965",
        },
        {
          id: "1b1b2cba-4a8f-40c0-a8b4-15747554270c",
          name: "FC Barcelona",
          alternativeNames: {
            "31504e64-98d6-445e-8392-b1b14c01bb4d": ["FC Barcelona"],
            "16642f69-68c6-4bed-88db-c4e6c33c737f": ["FC BARCELONA"],
          },
          teamId: "10c59325-6ba4-48ae-b17d-ae8685d36d9d",
          matchId: "45b1484f-751f-4862-ad5e-1322d0e79965",
        },
      ],
      competition: {
        id: "46e6090e-ec84-480b-9d37-d8f1e27d0f79",
        name: "La Liga",
        alternativeNames: {
          "16642f69-68c6-4bed-88db-c4e6c33c737f": ["La Liga"],
          "aaf4ea31-c643-4142-adce-e3577118adfc": ["LA LIGA"],
        },
        sport: {
          id: "7dca3c22-5dda-4806-8f09-a355b77f6bcc",
          name: "Fußball",
          alternativeNames: {
            "16642f69-68c6-4bed-88db-c4e6c33c737f": ["FUSSBALL"],
            "31504e64-98d6-445e-8392-b1b14c01bb4d": ["Fußball"],
          },
        },
        country: {
          id: "a2b913d0-77ba-43b2-a8a6-123e525cd68b",
          name: "Spanien",
          alternativeNames: {
            "16642f69-68c6-4bed-88db-c4e6c33c737f": ["SPANIEN"],
            "31504e64-98d6-445e-8392-b1b14c01bb4d": ["Spanien"],
          },
        },
      },
    },
    betType: {
      id: "592fecca-9a9b-4ea0-bb89-5c9078c946c5",
      name: "Tendency after Regular Playing Time",
      outcomeNames: [
        {
          id: "b625073e-099b-4876-bb70-9dd112c47a6a",
          name: "1",
        },
        {
          id: "2972466c-6952-4c80-888b-c840942456ec",
          name: "X",
        },
        {
          id: "c756965f-9a64-4935-89d8-124de0f7f698",
          name: "2",
        },
      ],
    },
    bets: [
      {
        profit: 0.0153846,
        isAvailable: true,
        outcomes: {
          1: {
            bookmaker: {
              id: "31504e64-98d6-445e-8392-b1b14c01bb4d",
              tax: "5% WINNINGS",
              name: "Bwin",
            },
            odd: 3.3,
          },
          2: {
            bookmaker: {
              id: "31504e64-98d6-445e-8392-b1b14c01bb4d",
              tax: "5% WINNINGS",
              name: "Bwin",
            },
            odd: 2.2,
          },
          X: {
            bookmaker: {
              id: "16642f69-68c6-4bed-88db-c4e6c33c737f",
              tax: "NONE",
              name: "Tipico",
            },
            odd: 4.4,
          },
        },
      },
    ],
  },
  {
    id: "49ef5d5c-9134-4125-9f36-479f75878a84",
    isAvailable: true,
    match: {
      id: "55b1484f-751f-4862-ad5e-1322d0e79966",
      name: "Real Madrid - FC Barcelona",
      result: null,
      status: "NS",
      currentMinute: null,
      startDate: 1620852557836,
      endDate: null,
      odds: [
        {
          id: "95b87467-2690-491f-9f46-d488fbdef597",
          name: "Odds 95b87467-2690-491f-9f46-d488fbdef597",
          bookmaker: {
            id: "31504e64-98d6-445e-8392-b1b14c01bb4d",
            tax: "5% WINNINGS",
            name: "Bwin",
          },
          bets: [
            {
              id: "785e2120-a43b-4e95-b486-058d399c3cb6",
              date: 1620752557836,
              type: {
                id: "592fecca-9a9b-4ea0-bb89-5c9078c946c5",
                name: "Tendency after Regular Playing Time",
                outcomeNames: [
                  {
                    id: "b625073e-099b-4876-bb70-9dd112c47a6a",
                    name: "1",
                  },
                  {
                    id: "2972466c-6952-4c80-888b-c840942456ec",
                    name: "X",
                  },
                  {
                    id: "c756965f-9a64-4935-89d8-124de0f7f698",
                    name: "2",
                  },
                ],
              },
              outcomes: [
                {
                  name: "b625073e-099b-4876-bb70-9dd112c47a6a",
                  odd: 3.3,
                  date: 1620752557836,
                },
                {
                  name: "2972466c-6952-4c80-888b-c840942456ec",
                  odd: 4,
                  date: 1620752557836,
                },
                {
                  name: "c756965f-9a64-4935-89d8-124de0f7f698",
                  odd: 2.2,
                  date: 1620752557836,
                },
              ],
            },
          ],
        },
        {
          id: "7bf63aee-8e08-41d2-9779-7a69e76e1135",
          name: "Odds 7bf63aee-8e08-41d2-9779-7a69e76e1135",
          bookmaker: {
            id: "16642f69-68c6-4bed-88db-c4e6c33c737f",
            tax: "NONE",
            name: "Tipico",
          },
          bets: [
            {
              id: "19dedd97-5c60-4ca7-a305-bf01e5641166",
              date: 1620752557836,
              type: {
                id: "592fecca-9a9b-4ea0-bb89-5c9078c946c5",
                name: "Tendency after Regular Playing Time",
                outcomeNames: [
                  {
                    id: "b625073e-099b-4876-bb70-9dd112c47a6a",
                    name: "1",
                  },
                  {
                    id: "2972466c-6952-4c80-888b-c840942456ec",
                    name: "X",
                  },
                  {
                    id: "c756965f-9a64-4935-89d8-124de0f7f698",
                    name: "2",
                  },
                ],
              },
              outcomes: [
                {
                  name: "b625073e-099b-4876-bb70-9dd112c47a6a",
                  odd: 3.5,
                  date: 1620752557836,
                },
                {
                  name: "2972466c-6952-4c80-888b-c840942456ec",
                  odd: 4.4,
                  date: 1620752557836,
                },
                {
                  name: "c756965f-9a64-4935-89d8-124de0f7f698",
                  odd: 1.8,
                  date: 1620752557836,
                },
              ],
            },
          ],
        },
      ],
      teams: [
        {
          id: "df380ea0-70f9-4a15-a5b9-ae3654d02c75",
          name: "Real Madrid",
          alternativeNames: {
            "31504e64-98d6-445e-8392-b1b14c01bb4d": ["Real Madrid"],
            "16642f69-68c6-4bed-88db-c4e6c33c737f": ["REAL MADRID"],
          },
          teamId: "7e81b45e-3fb7-468c-85e0-ff68188c8233",
          matchId: "45b1484f-751f-4862-ad5e-1322d0e79965",
        },
        {
          id: "1b1b2cba-4a8f-40c0-a8b4-15747554270c",
          name: "FC Barcelona",
          alternativeNames: {
            "31504e64-98d6-445e-8392-b1b14c01bb4d": ["FC Barcelona"],
            "16642f69-68c6-4bed-88db-c4e6c33c737f": ["FC BARCELONA"],
          },
          teamId: "10c59325-6ba4-48ae-b17d-ae8685d36d9d",
          matchId: "45b1484f-751f-4862-ad5e-1322d0e79965",
        },
      ],
      competition: {
        id: "46e6090e-ec84-480b-9d37-d8f1e27d0f79",
        name: "La Liga",
        alternativeNames: {
          "16642f69-68c6-4bed-88db-c4e6c33c737f": ["La Liga"],
          "aaf4ea31-c643-4142-adce-e3577118adfc": ["LA LIGA"],
        },
        sport: {
          id: "7dca3c22-5dda-4806-8f09-a355b77f6bcc",
          name: "Fußball",
          alternativeNames: {
            "16642f69-68c6-4bed-88db-c4e6c33c737f": ["FUSSBALL"],
            "31504e64-98d6-445e-8392-b1b14c01bb4d": ["Fußball"],
          },
        },
        country: {
          id: "a2b913d0-77ba-43b2-a8a6-123e525cd68b",
          name: "Spanien",
          alternativeNames: {
            "16642f69-68c6-4bed-88db-c4e6c33c737f": ["SPANIEN"],
            "31504e64-98d6-445e-8392-b1b14c01bb4d": ["Spanien"],
          },
        },
      },
    },
    betType: {
      id: "592fecca-9a9b-4ea0-bb89-5c9078c946c5",
      name: "Tendency after Regular Playing Time",
      outcomeNames: [
        {
          id: "b625073e-099b-4876-bb70-9dd112c47a6a",
          name: "1",
        },
        {
          id: "2972466c-6952-4c80-888b-c840942456ec",
          name: "X",
        },
        {
          id: "c756965f-9a64-4935-89d8-124de0f7f698",
          name: "2",
        },
      ],
    },
    bets: [
      {
        profit: 0.0153846,
        isAvailable: true,
        outcomes: {
          1: {
            bookmaker: {
              id: "31504e64-98d6-445e-8392-b1b14c01bb4d",
              tax: "5% WINNINGS",
              name: "Bwin",
            },
            odd: 3.3,
          },
          2: {
            bookmaker: {
              id: "31504e64-98d6-445e-8392-b1b14c01bb4d",
              tax: "5% WINNINGS",
              name: "Bwin",
            },
            odd: 2.2,
          },
          X: {
            bookmaker: {
              id: "16642f69-68c6-4bed-88db-c4e6c33c737f",
              tax: "NONE",
              name: "Tipico",
            },
            odd: 4.4,
          },
        },
      },
    ],
  },
  {
    id: "49ef5d5c-9134-4125-9f36-479f75878a85",
    isAvailable: true,
    match: {
      id: "55b1484f-751f-4862-ad5e-1322d0e79966",
      name: "Real Madrid - FC Barcelona",
      result: null,
      status: "NS",
      currentMinute: null,
      startDate: 1620852557836,
      endDate: null,
      odds: [
        {
          id: "95b87467-2690-491f-9f46-d488fbdef597",
          name: "Odds 95b87467-2690-491f-9f46-d488fbdef597",
          bookmaker: {
            id: "31504e64-98d6-445e-8392-b1b14c01bb4d",
            tax: "5% WINNINGS",
            name: "Bwin",
          },
          bets: [
            {
              id: "785e2120-a43b-4e95-b486-058d399c3cb6",
              date: 1620752557836,
              type: {
                id: "592fecca-9a9b-4ea0-bb89-5c9078c946c5",
                name: "Tendency after Regular Playing Time",
                outcomeNames: [
                  {
                    id: "b625073e-099b-4876-bb70-9dd112c47a6a",
                    name: "1",
                  },
                  {
                    id: "2972466c-6952-4c80-888b-c840942456ec",
                    name: "X",
                  },
                  {
                    id: "c756965f-9a64-4935-89d8-124de0f7f698",
                    name: "2",
                  },
                ],
              },
              outcomes: [
                {
                  name: "b625073e-099b-4876-bb70-9dd112c47a6a",
                  odd: 3.3,
                  date: 1620752557836,
                },
                {
                  name: "2972466c-6952-4c80-888b-c840942456ec",
                  odd: 4,
                  date: 1620752557836,
                },
                {
                  name: "c756965f-9a64-4935-89d8-124de0f7f698",
                  odd: 2.2,
                  date: 1620752557836,
                },
              ],
            },
          ],
        },
        {
          id: "7bf63aee-8e08-41d2-9779-7a69e76e1135",
          name: "Odds 7bf63aee-8e08-41d2-9779-7a69e76e1135",
          bookmaker: {
            id: "16642f69-68c6-4bed-88db-c4e6c33c737f",
            tax: "NONE",
            name: "Tipico",
          },
          bets: [
            {
              id: "19dedd97-5c60-4ca7-a305-bf01e5641166",
              date: 1620752557836,
              type: {
                id: "592fecca-9a9b-4ea0-bb89-5c9078c946c5",
                name: "Tendency after Regular Playing Time",
                outcomeNames: [
                  {
                    id: "b625073e-099b-4876-bb70-9dd112c47a6a",
                    name: "1",
                  },
                  {
                    id: "2972466c-6952-4c80-888b-c840942456ec",
                    name: "X",
                  },
                  {
                    id: "c756965f-9a64-4935-89d8-124de0f7f698",
                    name: "2",
                  },
                ],
              },
              outcomes: [
                {
                  name: "b625073e-099b-4876-bb70-9dd112c47a6a",
                  odd: 3.5,
                  date: 1620752557836,
                },
                {
                  name: "2972466c-6952-4c80-888b-c840942456ec",
                  odd: 4.4,
                  date: 1620752557836,
                },
                {
                  name: "c756965f-9a64-4935-89d8-124de0f7f698",
                  odd: 1.8,
                  date: 1620752557836,
                },
              ],
            },
          ],
        },
      ],
      teams: [
        {
          id: "df380ea0-70f9-4a15-a5b9-ae3654d02c75",
          name: "Real Madrid",
          alternativeNames: {
            "31504e64-98d6-445e-8392-b1b14c01bb4d": ["Real Madrid"],
            "16642f69-68c6-4bed-88db-c4e6c33c737f": ["REAL MADRID"],
          },
          teamId: "7e81b45e-3fb7-468c-85e0-ff68188c8233",
          matchId: "45b1484f-751f-4862-ad5e-1322d0e79965",
        },
        {
          id: "1b1b2cba-4a8f-40c0-a8b4-15747554270c",
          name: "FC Barcelona",
          alternativeNames: {
            "31504e64-98d6-445e-8392-b1b14c01bb4d": ["FC Barcelona"],
            "16642f69-68c6-4bed-88db-c4e6c33c737f": ["FC BARCELONA"],
          },
          teamId: "10c59325-6ba4-48ae-b17d-ae8685d36d9d",
          matchId: "45b1484f-751f-4862-ad5e-1322d0e79965",
        },
      ],
      competition: {
        id: "46e6090e-ec84-480b-9d37-d8f1e27d0f79",
        name: "La Liga",
        alternativeNames: {
          "16642f69-68c6-4bed-88db-c4e6c33c737f": ["La Liga"],
          "aaf4ea31-c643-4142-adce-e3577118adfc": ["LA LIGA"],
        },
        sport: {
          id: "7dca3c22-5dda-4806-8f09-a355b77f6bcc",
          name: "Fußball",
          alternativeNames: {
            "16642f69-68c6-4bed-88db-c4e6c33c737f": ["FUSSBALL"],
            "31504e64-98d6-445e-8392-b1b14c01bb4d": ["Fußball"],
          },
        },
        country: {
          id: "a2b913d0-77ba-43b2-a8a6-123e525cd68b",
          name: "Spanien",
          alternativeNames: {
            "16642f69-68c6-4bed-88db-c4e6c33c737f": ["SPANIEN"],
            "31504e64-98d6-445e-8392-b1b14c01bb4d": ["Spanien"],
          },
        },
      },
    },
    betType: {
      id: "592fecca-9a9b-4ea0-bb89-5c9078c946c5",
      name: "Tendency after Regular Playing Time",
      outcomeNames: [
        {
          id: "b625073e-099b-4876-bb70-9dd112c47a6a",
          name: "1",
        },
        {
          id: "2972466c-6952-4c80-888b-c840942456ec",
          name: "X",
        },
        {
          id: "c756965f-9a64-4935-89d8-124de0f7f698",
          name: "2",
        },
      ],
    },
    bets: [
      {
        profit: 0.0153846,
        isAvailable: true,
        outcomes: {
          1: {
            bookmaker: {
              id: "31504e64-98d6-445e-8392-b1b14c01bb4d",
              tax: "5% WINNINGS",
              name: "Bwin",
            },
            odd: 3.3,
          },
          2: {
            bookmaker: {
              id: "31504e64-98d6-445e-8392-b1b14c01bb4d",
              tax: "5% WINNINGS",
              name: "Bwin",
            },
            odd: 2.2,
          },
          X: {
            bookmaker: {
              id: "16642f69-68c6-4bed-88db-c4e6c33c737f",
              tax: "NONE",
              name: "Tipico",
            },
            odd: 4.4,
          },
        },
      },
    ],
  },
];
