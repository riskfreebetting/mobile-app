import React from "react";
import { StyleSheet, View } from "react-native";
import ComingSoon from "../../components/ComingSoon";
import { OpenableRFBTextPanel } from "../../components/RFBPanel";
import TopBar from "../../components/TopBar";

function TutorialsScreen() {
  return (
    <View>
      <TopBar style={styles.topBar} />
      <OpenableRFBTextPanel isOpened={true} text="Tutorial Videos">
        <ComingSoon style={styles.comingSoon} />
      </OpenableRFBTextPanel>
    </View>
  );
}

const styles = StyleSheet.create({
  comingSoon: {
    marginTop: 35,
  },
  topBar: {
    marginBottom: 20,
  },
});

export default TutorialsScreen;
