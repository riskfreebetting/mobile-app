import React, { Component } from "react";
import {
  FlatList,
  SafeAreaView,
  StyleSheet,
  TouchableWithoutFeedback,
  View,
} from "react-native";
import { LinearGradient } from "expo-linear-gradient";

import colors from "../../config/colors";
import { getBookmakerName, getStatusBarHeight, sum } from "../../scripts/utils";
import CalculatorOutcomeEntry from "./CalculatorOutcomeEntry";
import RFBCalculator from "./RFBCalculator";
import RFBText from "../../components/RFBText";

class OpenedRFB extends Component {
  constructor(props) {
    super(props);

    this.rfb = props.rfb;
    this.onCloseRFB = props.onCloseRFB;

    const initTotalStake = 100;
    const initRounding = 0.5;
    const bets = props.rfb.bets;

    const selectedBet = this.findMostProfitableBet(bets);
    const winnings = this.calculateWinnings(selectedBet);

    this.state = {
      bets,
      selectedBet,
      winnings,
      totalStake: initTotalStake,
      outcomeData: this.calculateOutcomeDataWithProfits(
        selectedBet.outcomes,
        winnings,
        initTotalStake,
        initRounding
      ),
      rounding: initRounding,
    };
  }

  findMostProfitableBet(bets) {
    return bets.reduce(
      (acc, bet) =>
        !acc || (bet.isAvailable && bet.profit > acc.profit) ? bet : acc,
      null
    );
  }

  calculateOutcomeDataWithProfits(outcomes, winnings, totalStake, rounding) {
    const outcomeDataWithoutProfit = this.calculateOutcomeData(
      outcomes,
      winnings,
      totalStake,
      rounding
    );

    return this.addProfitToOutcomeData(outcomeDataWithoutProfit, totalStake);
  }

  updateSelectedBet(selectedBet) {
    const newWinnings = this.calculateWinnings(selectedBet);

    this.updateOutcomeData(
      selectedBet.outcomes,
      newWinnings,
      this.state.totalStake,
      this.state.rounding
    );

    this.setState({
      selectedBet,
      winnings: newWinnings,
    });
  }

  calculateWinnings(bet) {
    const profitOfBookmaker = sum((outcome) => 1 / this.applyFeeToOdd(outcome))(
      Object.values(bet.outcomes)
    );

    return isFinite(profitOfBookmaker) ? 1 / profitOfBookmaker : 0;
  }

  updateOutcomeData(outcomes, winnings, totalStake, rounding) {
    const outcomeDataWithoutProfit = this.calculateOutcomeData(
      outcomes,
      winnings,
      totalStake,
      rounding
    );

    const newTotalStake = sum(({ stake }) => stake)(
      Object.values(outcomeDataWithoutProfit)
    );

    const outcomeData = this.addProfitToOutcomeData(
      outcomeDataWithoutProfit,
      newTotalStake
    );

    this.setState({
      outcomeData,
      totalStake: newTotalStake,
    });
  }

  calculateOutcomeData(outcomes, winnings, totalStake, rounding) {
    return Object.entries(outcomes).reduce((acc, [outcomeId, outcome]) => {
      const odd = outcome.odd;
      const fee = outcome.fee;
      const oddAfterFee = this.applyFeeToOdd(outcome);
      const unroundedStake = totalStake * (winnings / oddAfterFee);
      const finiteUnroundedStake = isFinite(unroundedStake)
        ? unroundedStake
        : 0;

      const stake = this.round(finiteUnroundedStake, rounding);

      return {
        ...acc,
        [outcomeId]: {
          bookmakerId: outcome.bookmaker.id,
          odd,
          fee,
          oddAfterFee,
          stake,
        },
      };
    }, {});
  }

  addProfitToOutcomeData(outcomeData, totalStake) {
    return Object.entries(outcomeData).reduce((acc, [outcomeId, outcome]) => {
      const { stake, oddAfterFee } = outcome;

      return {
        ...acc,
        [outcomeId]: {
          ...outcome,
          profit: stake * oddAfterFee - totalStake,
        },
      };
    }, {});
  }

  applyFeeToOdd({ odd, fee }) {
    return odd * (1 - fee);
  }

  round(stake, rounding) {
    rounding = rounding || 0.01;
    return Math.round(stake / rounding) * rounding;
  }

  render() {
    return (
      <SafeAreaView style={styles.safeAreaViewContainer}>
        <View style={styles.container}>
          <View style={styles.contentContainer}>
            <FlatList
              data={this.rfb.betType.outcomeNames}
              key="id"
              ListHeaderComponent={renderHeader(this.rfb.betType)}
              ItemSeparatorComponent={Seperator}
              renderItem={this.renderOutcomeItem(this.rfb)}
              ListFooterComponent={this.renderCalculator()}
              ListFooterComponentStyle={styles.footer}
              extraData={this.rfb}
            />
          </View>
          <TouchableWithoutFeedback onPress={this.onCloseRFB}>
            <LinearGradient
              style={styles.fadeContainer}
              colors={[colors.background, "transparent"]}
            />
          </TouchableWithoutFeedback>
        </View>
      </SafeAreaView>
    );
  }

  renderOutcomeItem(rfb) {
    return ({ item: outcome }) => (
      <CalculatorOutcomeEntry
        rfb={rfb}
        outcome={outcome}
        outcomeData={this.state.outcomeData[outcome.name]}
        onPlaceBetPressed={this.handlePlaceBetPressed.bind(this)}
      />
    );
  }

  renderCalculator() {
    return (
      <RFBCalculator
        rfb={this.rfb}
        bets={this.state.bets}
        bet={this.state.selectedBet}
        outcomeData={this.state.outcomeData}
        winnings={this.state.winnings}
        rounding={this.state.rounding}
        totalStake={this.state.totalStake}
        onBookmakerChange={this.handleBookmakerChange.bind(this)}
        onFeeChange={this.handleFeeChanged.bind(this)}
        onStakeChange={this.handleStakeChanged.bind(this)}
        onTotalStakeChange={this.handleTotalStakeChanged.bind(this)}
        onRoundingChange={this.handleRoundingChanged.bind(this)}
      />
    );
  }

  handlePlaceBetPressed() {
    alert(
      "Sorry, this feature is not implemented yet. Please find the bet yourself."
    );
  }

  handleBookmakerChange(outcomeName, bookmakerName) {
    if (
      getBookmakerName(outcomeName)(this.state.selectedBet) !== bookmakerName
    ) {
      const selectedBet = this.state.bets.find(
        (bet) => getBookmakerName(outcomeName)(bet) === bookmakerName
      );

      this.updateSelectedBet(selectedBet);
    }
  }

  handleFeeChanged(outcomeName, fee) {
    const updatedBet = {
      ...this.state.selectedBet,
      outcomes: {
        ...this.state.selectedBet.outcomes,
        [outcomeName]: {
          ...this.state.selectedBet.outcomes[outcomeName],
          fee,
        },
      },
    };

    this.updateSelectedBet(updatedBet);
  }

  handleStakeChanged(outcomeName, stake) {
    if (stake > 0) {
      const stakeBefore = this.state.outcomeData[outcomeName].stake;
      const ratio = stake / stakeBefore;

      const totalStake = this.state.totalStake * ratio;
      this.setState({
        totalStake,
      });

      this.updateOutcomeData(
        this.state.selectedBet.outcomes,
        this.state.winnings,
        totalStake,
        this.state.rounding
      );
    }
  }

  handleTotalStakeChanged(totalStake) {
    this.setState({
      totalStake,
    });

    this.updateOutcomeData(
      this.state.selectedBet.outcomes,
      this.state.winnings,
      totalStake,
      this.state.rounding
    );
  }

  handleRoundingChanged(rounding) {
    this.setState({
      rounding,
    });

    this.updateOutcomeData(
      this.state.selectedBet.outcomes,
      this.state.winnings,
      this.state.totalStake,
      rounding
    );
  }

  calculateProfit(outcomeName, stake) {
    const returnMoney = stake * rfb.bets[0].outcomes[outcomeName].odd;
    const totalStake = this.state.totalStake;

    return returnMoney - totalStake;
  }
}

function renderHeader(betType) {
  return (
    <View style={styles.headerContainer}>
      <RFBText style={styles.title}>{betType.name}:</RFBText>
    </View>
  );
}

function Seperator() {
  return <View style={styles.seperator} />;
}

const styles = StyleSheet.create({
  safeAreaViewContainer: {
    position: "absolute",
    width: "100%",
    height: "100%",
    left: 0,
    right: 0,
    top: 0,
    flex: 1,
    marginTop: getStatusBarHeight(),
  },
  container: {
    flex: 1,
  },
  contentContainer: {
    backgroundColor: colors.background2,
    flex: 9,
    justifyContent: "flex-start",
    borderRadius: 10,
    zIndex: 3,
  },
  fadeContainer: {
    flex: 1,
    top: -10,
    zIndex: 2,
  },
  footer: {
    marginTop: 25,
  },
  headerContainer: {
    margin: 10,
  },
  seperator: {
    height: 10,
  },
  title: {},
});

export default OpenedRFB;
