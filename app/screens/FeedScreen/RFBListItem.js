import React from "react";

import { StyleSheet, View } from "react-native";
import colors from "../../config/colors";

import ListItem from "../../components/ListItem";
import MatchDataHeader from "../../components/MatchDataHeader";
import MatchDataRow from "../../components/MatchDataRow";
import RFBText from "../../components/RFBText";

function RFBListItem(props) {
  const { rfb, onRFBOpened } = props;
  return (
    <ListItem
      {...props}
      HeaderComponent={<MatchDataHeader rfb={rfb} />}
      ContentComponent={renderOutcomeList(rfb)}
      FooterComponent={renderFooter(rfb)}
      buttonText="Open"
      onPress={() => onRFBOpened(rfb)}
    />
  );
}

function renderOutcomeList(rfb) {
  return (
    <View style={styles.outcomesContainer}>
      {rfb?.betType?.outcomeNames?.map?.((outcome) => (
        <MatchDataRow key={outcome.id} rfb={rfb} outcome={outcome} />
      ))}
    </View>
  );
}

function renderFooter(rfb) {
  return (
    <RFBText style={styles.profitText}>
      {getFormattedProfit(rfb.bets[0].profit)} Profit
    </RFBText>
  );
}

function getFormattedProfit(profit) {
  return Math.round(profit * 10000) / 100 + "%";
}

const styles = StyleSheet.create({
  outcomesContainer: {
    paddingHorizontal: 10,
    paddingVertical: 3,
    justifyContent: "space-evenly",
  },
  profitText: {
    color: colors.accent,
    paddingVertical: 5,
  },
});

export default RFBListItem;
