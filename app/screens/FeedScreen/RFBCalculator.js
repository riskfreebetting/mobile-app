import React from "react";
import { StyleSheet, View } from "react-native";
import ComboBox from "../../components/ComboBox";
import { MoneyInput, PercentageInput } from "../../components/Input";
import RFBText from "../../components/RFBText";
import colors from "../../config/colors";
import { fontSizes } from "../../config/fonts";
import {
  convertNumberToString,
  convertOddsNumberToString,
  getBookmakerName,
} from "../../scripts/utils";

const RFBCalculator = ({
  rfb,
  bet,
  bets,
  outcomeData,
  winnings,
  rounding,
  totalStake,
  onBookmakerChange,
  onFeeChange,
  onStakeChange,
  onTotalStakeChange,
  onRoundingChange,
}) => {
  return (
    <>
      <View style={styles.headerContainer}>{renderHeaderContainer()}</View>
      <View
        style={[styles.contentAndFooterContainer, styles.lighterBackground]}
      >
        {renderOutcomesTable(
          rfb.betType,
          bets,
          bet,
          outcomeData,
          onBookmakerChange,
          onFeeChange,
          onStakeChange
        )}
        {renderFooter(
          winnings,
          totalStake,
          rounding,
          onTotalStakeChange,
          onRoundingChange
        )}
      </View>
    </>
  );
};

const renderHeaderContainer = () => (
  <>
    <View style={styles.titleContainer}>
      <RFBText style={styles.title}>Calculator:</RFBText>
    </View>
    <View style={styles.seperator} />
    <View style={[styles.feeTitleContainer, styles.lighterBackground]}>
      <RFBText style={styles.feeTitle}>Fee</RFBText>
    </View>
    <View style={styles.seperator} />
    <View style={[styles.stakeTitleContainer, styles.lighterBackground]}>
      <RFBText style={styles.stakeTitle}>Stake</RFBText>
    </View>
    <View style={styles.seperator} />
    <View style={[styles.profitTitleContainer, styles.lighterBackground]}>
      <RFBText style={styles.profitTitle}>Profit</RFBText>
    </View>
    <View style={styles.tinySeperator} />
  </>
);

const renderOutcomesTable = (
  betType,
  bets,
  selectedBet,
  outcomeData,
  handleSelectBookmaker,
  handleFeeChange,
  handleStakeChange
) => {
  return betType.outcomeNames.map(({ id, name: outcomeName }) => {
    const outcome = outcomeData[outcomeName];

    return (
      <View
        key={id}
        style={[styles.outcomeContainer, styles.lighterBackground]}
      >
        <View style={styles.outcomeNameContainer}>
          <RFBText style={styles.outcomeName}>{outcomeName}</RFBText>
        </View>
        <View style={styles.bookmakerContainer}>
          <ComboBox
            items={getPossibleBookmakerNames(bets, outcomeName)}
            selectedValue={getBookmakerName(outcomeName)(selectedBet)}
            onSelect={(bookmaker) =>
              handleSelectBookmaker(outcomeName, bookmaker)
            }
            color={colors.primary}
          />
        </View>
        <View style={styles.oddContainer}>
          <RFBText style={styles.odd} adjustsFontSizeToFit numberOfLines={1}>
            {convertOddsNumberToString(outcome?.odd)}
          </RFBText>
        </View>
        <View style={styles.feeContainer}>
          <PercentageInput
            value={outcome?.fee}
            style={styles.fee}
            onEndEditing={(fee) => handleFeeChange(outcomeName, fee)}
          />
        </View>
        <View style={styles.oddAfterFeeContainer}>
          <RFBText
            style={styles.oddAfterFee}
            adjustsFontSizeToFit
            numberOfLines={1}
          >
            {convertOddsNumberToString(outcome?.oddAfterFee)}
          </RFBText>
        </View>
        <View style={styles.stakeContainer}>
          <MoneyInput
            value={outcome?.stake}
            color={colors.primary}
            onEndEditing={(stake) => handleStakeChange(outcomeName, stake)}
          />
        </View>
        <View style={styles.seperator} />
        <View style={styles.profitContainer}>
          <RFBText style={styles.profit} adjustsFontSizeToFit numberOfLines={1}>
            {convertNumberToString(outcome?.profit)} €
          </RFBText>
        </View>
        <View style={styles.tinySeperator} />
      </View>
    );
  });
};

const getPossibleBookmakerNames = (bets, outcomeName) => [
  ...new Set(bets.map(getBookmakerName(outcomeName))),
];

const renderFooter = (
  winnings,
  totalStake,
  rounding,
  handleTotalStakeChange,
  handleRoundingChange
) => {
  return (
    <View style={styles.footerContainer}>
      <View style={styles.roundingTextContainer}>
        <RFBText style={styles.roundingText}>Rounding:</RFBText>
      </View>
      <View style={styles.roundingInputContainer}>
        <MoneyInput
          style={styles.roundingInput}
          value={rounding}
          color={colors.primary}
          onEndEditing={handleRoundingChange}
        />
      </View>
      <View style={styles.seperator} />
      <View style={styles.totalStakeContainer}>
        <MoneyInput
          value={totalStake}
          color={colors.primary}
          onEndEditing={handleTotalStakeChange}
        />
      </View>
      <View style={styles.seperator} />
      <View style={styles.totalProfitContainer}>
        <RFBText
          style={styles.totalProfitText}
          adjustsFontSizeToFit
          numberOfLines={1}
        >
          {convertNumberToString((winnings - 1) * 100)} %
        </RFBText>
      </View>
      <View style={styles.tinySeperator} />
    </View>
  );
};

const styles = StyleSheet.create({
  bookmakerContainer: {
    flex: 3,
    alignItems: "flex-start",
  },
  contentAndFooterContainer: {
    paddingVertical: 10,
    shadowColor: colors.black,
    shadowRadius: 2,
    shadowOpacity: 0.3,
    borderRadius: 20,
    shadowOffset: {
      width: 0,
      height: 5,
    },
  },
  fee: {},
  feeContainer: {
    flex: 1.5,
    alignItems: "center",
  },
  feeTitle: {
    color: colors.accent2,
    padding: 4,
    fontSize: fontSizes.XXS,
  },
  feeTitleContainer: {
    flex: 1.5,
    alignItems: "center",
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
  },
  footerContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingTop: 5,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
  },
  headerContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  outcomeContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingVertical: 5,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  outcomeName: {
    fontSize: fontSizes.XXS,
  },
  outcomeNameContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  odd: {
    color: colors.accent2,
  },
  oddAfterFee: {
    color: colors.accent3,
  },
  oddAfterFeeContainer: {
    flex: 1.25,
    alignItems: "center",
    justifyContent: "center",
  },
  oddContainer: {
    flex: 1.25,
    alignItems: "center",
    justifyContent: "center",
  },
  profit: {
    color: colors.accent,
  },
  profitContainer: {
    flex: 2,
    alignItems: "center",
    justifyContent: "center",
  },
  profitTitle: {
    color: colors.accent,
    padding: 4,
    fontSize: fontSizes.XXS,
  },
  profitTitleContainer: {
    flex: 2,
    alignItems: "center",
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
  },
  roundingInputContainer: {
    flex: 2.5,
    alignItems: "flex-end",
  },
  roundingText: {
    marginRight: 10,
    fontSize: fontSizes.XXS,
  },
  roundingTextContainer: {
    flex: 4.5,
    alignItems: "flex-end",
    justifyContent: "center",
  },
  seperator: {
    flex: 1,
  },
  stake: {},
  stakeContainer: {
    flex: 2.5,
    alignItems: "flex-end",
  },
  stakeTitle: {
    color: colors.accent3,
    padding: 4,
    fontSize: fontSizes.XXS,
  },
  stakeTitleContainer: {
    flex: 2.5,
    alignItems: "center",
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
  },
  tinySeperator: {
    flex: 0.5,
  },
  title: {
    padding: 4,
    fontSize: fontSizes.XXS,
  },
  titleContainer: {
    flex: 4,
  },
  totalProfitContainer: {
    flex: 2,
    alignItems: "center",
    justifyContent: "center",
  },
  totalProfitText: {
    color: colors.accent,
  },
  totalStakeContainer: {
    flex: 2.5,
    alignItems: "flex-end",
  },
  totalStakeText: {},
  lighterBackground: {
    backgroundColor: colors.secondary,
  },
});

export default RFBCalculator;
