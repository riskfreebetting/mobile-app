import React from "react";

import { StyleSheet, View } from "react-native";
import colors from "../../config/colors";

import ListItem from "../../components/ListItem";
import MatchDataHeader from "../../components/MatchDataHeader";
import MatchDataRow from "../../components/MatchDataRow";
import { convertNumberToString } from "../../scripts/utils";
import RFBText from "../../components/RFBText";

function CalculatorOutcomeEntry({
  rfb,
  outcome,
  outcomeData,
  onPlaceBetPressed,
}) {
  return (
    <ListItem
      HeaderComponent={<MatchDataHeader rfb={rfb} />}
      ContentComponent={
        <View style={styles.outcomeContainer}>
          <MatchDataRow rfb={rfb} outcome={outcome} />
        </View>
      }
      FooterComponent={renderFooter(outcomeData?.stake, outcomeData?.profit)}
      buttonText="Place Bet"
      onPress={() => onPlaceBetPressed(rfb, outcome)}
      addShadow
    />
  );
}

function renderFooter(stake, profit) {
  return (
    <View style={styles.footerContainer}>
      <RFBText style={styles.stake} adjustsFontSizeToFit numberOfLines={1}>
        Stake: {convertNumberToString(stake)} €
      </RFBText>
      <RFBText style={styles.arrow} adjustsFontSizeToFit numberOfLines={1}>
        &#8594;
      </RFBText>
      <RFBText style={styles.profit} adjustsFontSizeToFit numberOfLines={1}>
        {convertNumberToString(profit)} €
      </RFBText>
    </View>
  );
}

const styles = StyleSheet.create({
  footerContainer: {
    flexDirection: "row",
    paddingLeft: 10,
  },
  outcomeContainer: {
    paddingHorizontal: 10,
    justifyContent: "space-evenly",
  },
  profit: {
    color: colors.accent,
  },
  rfbContainerHeader: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingVertical: 5,
    paddingHorizontal: 10,
  },
  stake: {
    color: colors.accent3,
  },
  arrow: {
    marginHorizontal: 8,
  },
});

export default CalculatorOutcomeEntry;
