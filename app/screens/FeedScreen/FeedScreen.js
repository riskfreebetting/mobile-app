import React from "react";
import {
  ActivityIndicator,
  FlatList,
  StyleSheet,
  TouchableOpacity,
  View,
} from "react-native";

import rfbs from "../../mock-data/rfbs";
import { fetchAvailableRFBsIfHasAny } from "../../scripts/requests";

import colors from "../../config/colors";
import { fontSizes } from "../../config/fonts";

import RFBListItem from "./RFBListItem";
import OpenedRFB from "./OpenedRFB";
import RFBText from "../../components/RFBText";
import TopBar from "../../components/TopBar";
import RFBSvgIcon from "../../components/RFBSvgIcon";

import Notifications from "../../assets/svg/icons/notifications.svg";
import NotificationsWithText from "../../assets/svg/icons/notificationsWithText.svg";

class FeedScreen extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      rfbs: [],
      isLoading: false,
      openedRFB: null,
    };

    this.rfbLoadingIntervalId = null;
  }

  componentDidMount() {
    this.reloadRFBs();

    this.rfbLoadingIntervalId = setInterval(
      this.reloadRFBs.bind(this),
      60 * 1000
    );
  }

  componentWillUnmount() {
    clearInterval(this.rfbLoadingIntervalId);
  }

  removeAndReloadRFBs() {
    this.setState({
      rfbs: [],
    });

    this.reloadRFBs();
  }

  async reloadRFBs() {
    this.setState({
      isLoading: true,
    });

    try {
      const rfbs = await fetchAvailableRFBsIfHasAny();
      const sortedRFBs = this.sortRFBs(rfbs).map(
        this.addDataToRFBOutcomes,
        this
      );

      console.log(sortedRFBs[0]);

      this.setState({
        isLoading: false,
        rfbs: sortedRFBs,
      });
    } catch (err) {
      console.error(err);
      this.setState({
        isLoading: false,
        rfbs,
      });
    }
  }

  sortRFBs(rfbs) {
    rfbs.sort((a, b) => b.bets[0].profit - a.bets[0].profit);

    return rfbs;
  }

  addDataToRFBOutcomes(rfb) {
    return {
      ...rfb,
      bets: rfb.bets
        .map(this.addFeeToBetOutcomes, this)
        .map(this.addTaxedOdd, this),
    };
  }

  addFeeToBetOutcomes(bet) {
    return this.mapOutcome((outcome) => ({
      fee: this.getFee(outcome),
    }))(bet);
  }

  getFee(outcome) {
    return outcome.bookmaker.tax === "5% WINNINGS" ? 0.05 : 0;
  }

  addTaxedOdd(bet) {
    return this.mapOutcome((outcome) => ({
      taxedOdd: outcome.odd * (1 - outcome.fee),
    }))(bet);
  }

  mapOutcome(fn) {
    return (bet) => ({
      ...bet,
      outcomes: Object.entries(bet.outcomes).reduce(
        (acc, [outcomeId, outcome]) => ({
          ...acc,
          [outcomeId]: {
            ...outcome,
            ...fn(outcome),
          },
        }),
        {}
      ),
    });
  }

  render() {
    const { openedRFB, rfbs, isLoading } = this.state;

    return (
      <>
        <TopBar buttons={this.renderButtons()} style={styles.topBar} />
        <FlatList
          data={rfbs}
          key="id"
          ListHeaderComponent={renderListHeader(rfbs)}
          ItemSeparatorComponent={Seperator}
          ListEmptyComponent={renderEmptyComponent(isLoading)}
          renderItem={this.renderRFB.bind(this)}
          onRefresh={this.removeAndReloadRFBs.bind(this)}
          refreshing={isLoading}
        />
        {openedRFB && this.renderOpenedRFB(openedRFB)}
      </>
    );
  }

  renderButtons() {
    return [
      () => (
        <TouchableOpacity
          activeOpacity={0.1}
          style={styles.notificationButtonContainer}
        >
          <RFBSvgIcon
            icon={NotificationsWithText}
            fill={colors.primary}
            fillSecondary={colors.accent3}
            width={40}
            height={40}
            alt="N"
          />
          <RFBText style={styles.notificationsText}>{Math.min(10, 99)}</RFBText>
        </TouchableOpacity>
      ),
    ];
  }

  renderRFB({ item: rfb }) {
    return (
      <RFBListItem
        rfb={rfb}
        onRFBOpened={(rfb) => this.handleRFBOpened(rfb)}
        backgroundColor={colors.background2}
        footerColor={colors.secondary}
      />
    );
  }

  renderOpenedRFB(openedRFB) {
    return (
      <OpenedRFB rfb={openedRFB} onCloseRFB={this.handleRFBClosed.bind(this)} />
    );
  }

  handleRFBOpened(openedRFB) {
    this.setState({ openedRFB });
  }

  handleRFBClosed() {
    this.setState({ openedRFB: null });
  }
}

function renderListHeader(rfbs) {
  return (
    <View style={styles.nrOfBetsTextsContainer}>
      <RFBText style={styles.nrOfBetsTextsNumber}>{rfbs.length}</RFBText>
      <RFBText style={styles.nrOfBetsTextsText}>
        RFB's found with active Filters
      </RFBText>
    </View>
  );
}

function Seperator() {
  return <View style={styles.seperator} />;
}

function renderEmptyComponent(isLoading) {
  return isLoading ? (
    <ActivityIndicator />
  ) : (
    <RFBText style={styles.noRFBsText}>
      There are no RFB's available with the current Filters...
    </RFBText>
  );
}

const styles = StyleSheet.create({
  nrOfBetsTextsContainer: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-start",
    padding: 10,
  },
  noRFBsText: {
    marginLeft: 10,
  },
  notificationButtonContainer: {
    alignItems: "center",
    justifyContent: "center",
  },
  notificationsText: {
    textAlign: "center",
    position: "absolute",
    width: "100%",
    left: 9.5,
    bottom: 13,
  },
  nrOfBetsTextsNumber: {
    color: colors.accent3,
    marginEnd: 4,
    fontSize: fontSizes.XXS,
  },
  nrOfBetsTextsText: {
    fontSize: fontSizes.XXS,
  },
  seperator: {
    height: 30,
  },
  topBar: {
    marginBottom: 10,
  },
});

export default FeedScreen;
