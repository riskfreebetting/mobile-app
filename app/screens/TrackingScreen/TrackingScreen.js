import React from "react";
import { StyleSheet, View } from "react-native";
import ComingSoon from "../../components/ComingSoon";
import TopBar from "../../components/TopBar";

import BulletPointIcon from "../../assets/svg/icons/bulletPoint.svg";
import { fontSizes } from "../../config/fonts";
import colors from "../../config/colors";
import RFBText from "../../components/RFBText";
import RFBSvgIcon from "../../components/RFBSvgIcon";

function TrackingScreen() {
  return (
    <View style={styles.container}>
      <TopBar style={styles.topBar} />
      <ComingSoon style={styles.comingSoon} />
      {renderContent()}
    </View>
  );
}

function renderContent() {
  return (
    <View style={styles.contentContainer}>
      <RFBText style={styles.subTitle}>What to expect:</RFBText>
      <View style={styles.bulletPointsContainer}>
        {BulletPoint("Track your profit with interactive graphs")}
        {BulletPoint("View all your previously placed RFBs")}
      </View>
    </View>
  );
}

function BulletPoint(text) {
  return (
    <View style={styles.bulletPointContainer}>
      <View style={styles.bulletPointIconContainer}>
        <RFBSvgIcon icon={BulletPointIcon} fill={colors.accent3} />
      </View>
      <View style={styles.bulletPointTextContainer}>
        <RFBText style={styles.bulletPointText}>{text}</RFBText>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    maxHeight: "100%",
    flex: 1,
  },
  bulletPointContainer: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-start",
    paddingVertical: 12.5,
  },
  bulletPointIconContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  bulletPointsContainer: {
    justifyContent: "space-evenly",
    alignItems: "center",
  },
  bulletPointText: {
    fontSize: fontSizes.M,
    color: colors.accent,
  },
  bulletPointTextContainer: {
    flex: 5,
    justifyContent: "center",
  },
  comingSoon: {
    marginBottom: 30,
  },
  contentContainer: {
    backgroundColor: colors.background2,
    width: "100%",
    borderRadius: 30,
    paddingHorizontal: 25,
    paddingVertical: 12.5,
    justifyContent: "space-evenly",
  },
  subTitle: {
    fontSize: fontSizes.L,
    color: colors.white,
    marginVertical: 12.5,
  },
  topBar: {
    marginBottom: 20,
  },
});

export default TrackingScreen;
