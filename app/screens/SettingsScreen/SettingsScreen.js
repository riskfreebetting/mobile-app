import React from "react";
import {
  Linking,
  Share,
  StyleSheet,
  TouchableOpacity,
  View,
} from "react-native";
import TopBar from "../../components/TopBar.js";
import RFBIconLink from "../../components/RFBIconLink.js";
import RFBText from "../../components/RFBText.js";
import RFBSvgIcon from "../../components/RFBSvgIcon.js";
import { RFBTextPanel } from "../../components/RFBPanel.js";

import colors from "../../config/colors.js";
import { fontSizes } from "../../config/fonts.js";

import YouTubeIcon from "../../assets/svg/logos/youtube.svg";
import FacebookIcon from "../../assets/svg/logos/facebook.svg";
import InstagramIcon from "../../assets/svg/logos/instagram.svg";
import TwitterIcon from "../../assets/svg/logos/twitter.svg";
import ShareIcon from "../../assets/svg/icons/share.svg";

function SettingsScreen() {
  return (
    <View style={styles.container}>
      <TopBar buttons={[ShareButton]} />
      <View style={styles.container}>
        <View style={styles.titleContainer}>
          <RFBText style={styles.title}>ABOUT US</RFBText>
        </View>
        <View style={styles.socialMediaContainer}>
          <View style={styles.linksContainer}>
            <RFBIconLink
              style={styles.socialMediaLink}
              icon={YouTubeIcon}
              href="https://www.youtube.com"
            >
              YouTube
            </RFBIconLink>
            <RFBIconLink
              style={styles.socialMediaLink}
              icon={FacebookIcon}
              href="https://www.facebook.com"
            >
              Facebook
            </RFBIconLink>
            <RFBIconLink
              style={styles.socialMediaLink}
              icon={InstagramIcon}
              href="https://www.instagram.com/riskfreebettingapp"
            >
              Instagram
            </RFBIconLink>
            <RFBIconLink
              style={styles.socialMediaLink}
              icon={TwitterIcon}
              href="https://www.twitter.com"
            >
              Twitter
            </RFBIconLink>
          </View>
          <View style={styles.socialMediaTextContainer}>
            <RFBText style={styles.socialMediaText}>
              Follow us on{" "}
              <RFBText style={[styles.socialMediaText, styles.bold]}>
                Social Media
              </RFBText>{" "}
              to stay updated!
            </RFBText>
          </View>
        </View>
        <View style={styles.descriptionsContainer}>
          <View style={styles.textContainer}>
            <RFBText style={styles.subTitle}>Who are we?</RFBText>
            <RFBText style={styles.paragraph}>
              We are a hard-working team commmited to our Company and our{" "}
              <RFBText style={[styles.paragraph, styles.bold]}>Users</RFBText>.
              We took matters into our hands when we learned about{"\n"}
              <RFBText style={[styles.paragraph, styles.bold]}>
                Risk Free Betting
              </RFBText>
              .
            </RFBText>
          </View>
          <View style={styles.textContainer}>
            <RFBText style={styles.subTitle}>Our Goals</RFBText>
            <RFBText style={styles.paragraph}>
              Our Goal is to create the most{" "}
              <RFBText style={[styles.paragraph, styles.bold]}>
                efficient
              </RFBText>
              , most{" "}
              <RFBText style={[styles.paragraph, styles.bold]}>
                intuitive
              </RFBText>{" "}
              and most{" "}
              <RFBText style={[styles.paragraph, styles.bold]}>
                reliable
              </RFBText>{" "}
              Risk Free Betting Software possible so that you can maximize your{" "}
              <RFBText style={[styles.paragraph, styles.bold]}>Profit</RFBText>.
            </RFBText>
          </View>
        </View>
      </View>
      <View style={styles.container}>
        <View style={styles.titleContainer}>
          <RFBText style={styles.title}>HELP & SUPPORT</RFBText>
        </View>
        <View style={styles.supportContainer}>
          <TouchableOpacity
            activeOpacity={0.6}
            style={styles.emailContainer}
            onPress={() => Linking.openURL("mailto:support@rfb.com")}
          >
            <RFBText style={styles.emailInnerContainer} selectable>
              support@rfb.com
            </RFBText>
          </TouchableOpacity>
          <View style={styles.supportTextContainer}>
            <RFBText style={styles.supportText}>
              <RFBText style={[styles.supportText, styles.big]}>
                Tell us about your{"\n"}
              </RFBText>
              Issues, Bugs & Feedback
            </RFBText>
          </View>
        </View>
        <RFBTextPanel color={colors.accent2}>
          Frequently Asked Questions
        </RFBTextPanel>
      </View>
      <View style={styles.footerContainer}>
        <RFBText>RFB v. 1.0.0</RFBText>
      </View>
    </View>
  );
}

function ShareButton() {
  return (
    <TouchableOpacity
      activeOpacity={0.6}
      onPress={() =>
        Share.share({
          title: "Share Risk Free Betting App To Friends",
          message:
            "Hey there,\nhave you heard of this cool new app where you can easily make money without any risk? You should try it out!\n",
          url: "https://apps.apple.com/de/app/make-money-earn-easy-cash/id1038856246",
        })
      }
    >
      <RFBSvgIcon
        icon={ShareIcon}
        fill={colors.primary}
        width={30}
        height={30}
      />
    </TouchableOpacity>
  );
}

/* function SettingsScreen() {
  return (
    <View>
      <SettingsTab title="USER" settings={["Account", "Premium Plan"]} />
      <SettingsTab title="APP" settings={["General", "Notifications"]} />
      <SettingsTab title="INFO" settings={["Help & Support", "General Terms & Conditions", "About us"]} />
    </View>
  );
}
 */

const styles = StyleSheet.create({
  bold: {
    fontWeight: "bold",
  },
  big: {
    fontSize: fontSizes.S,
  },
  container: {
    alignItems: "center",
    width: "100%",
    marginBottom: 25,
  },
  descriptionsContainer: {
    width: "100%",
    flexDirection: "row",
    justifyContent: "center",
  },
  emailContainer: {
    justifyContent: "center",
    alignItems: "center",
    flex: 1,
  },
  emailInnerContainer: {
    justifyContent: "center",
    alignItems: "center",
    paddingHorizontal: 10,
    paddingVertical: 5,
    borderColor: colors.secondary,
    borderRadius: 5,
    borderWidth: 1,
  },
  footerContainer: {
    width: "100%",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
  linksContainer: {
    flexDirection: "row",
    justifyContent: "space-evenly",
    flex: 10,
  },
  paragraph: {
    textAlign: "center",
    fontSize: fontSizes.XS,
  },
  socialMediaContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginHorizontal: 15,
    marginBottom: 25,
  },
  socialMediaLink: {
    flex: 1,
    width: "100%",
  },
  socialMediaTextContainer: {
    flex: 4,
  },
  socialMediaText: {
    color: colors.accent,
    fontSize: fontSizes.XS,
    textAlign: "center",
  },
  subTitle: {
    color: colors.accent,
    marginBottom: 10,
    fontSize: fontSizes.S,
    textAlign: "center",
  },
  supportContainer: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 25,
  },
  supportText: {
    color: colors.accent,
  },
  supportTextContainer: {
    flex: 1,
    justifyContent: "center",
    alignContent: "center",
  },
  textContainer: {
    flex: 1,
    paddingHorizontal: 5,
  },
  title: {
    fontSize: fontSizes.XS,
    marginLeft: 15,
  },
  titleContainer: {
    backgroundColor: colors.background2,
    height: 35,
    borderRadius: 5,
    justifyContent: "center",
    marginBottom: 18,
    minWidth: "100%",
  },
});

export default SettingsScreen;
