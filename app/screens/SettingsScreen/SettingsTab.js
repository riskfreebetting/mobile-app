import React, { useState } from "react";
import { StyleSheet, View } from "react-native";
import RFBText from "../../components/RFBText";

function SettingsTab(props) {
  return (
    <View>
      <View>
        <RFBText>{props.title}</RFBText>
      </View>
      <View>
        {props.settings.map(function (setting) {
          return (
            <View>
              <RFBText>{setting}</RFBText>
            </View>
          );
        })}
      </View>
    </View>
  );
}

const styles = StyleSheet.create({});

export default SettingsTab;
