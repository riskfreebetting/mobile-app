import React from "react";
import { Platform, SafeAreaView, StatusBar, StyleSheet } from "react-native";
import { fontsToLoad } from "./app/config/fonts";
import { useFonts } from "@expo-google-fonts/rubik";
import AppLoading from "expo-app-loading";
import ScreenRouter from "./app/components/ScreenRouter";
import RFBWebView from "./app/components/RFBWebView";
import colors from "./app/config/colors";
import AppStateProvider from "./app/components/AppStateProvider";

export default () => {
  const [areFontsLoaded] = useFonts(fontsToLoad);

  return (
    <AppStateProvider>
      {areFontsLoaded ? (
        <SafeAreaView style={styles.container}>
          <ScreenRouter />
          <RFBWebView />
        </SafeAreaView>
      ) : (
        <AppLoading />
      )}
    </AppStateProvider>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.background,
    paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
  },
});
